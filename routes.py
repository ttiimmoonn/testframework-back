from handler.handler import Handler

# Формируем api сервера 
def setup_routes(app, logger):
    # Формируем класс обработчиков
    logger.debug('We form a class handler...')
    try:
        handler = Handler(logger)
    except Exception as e:
        logger.error('Error form a class handler.')
        logger.error(e)

    routes = [
        # Список всех тестов
        ('GET', '/api/tests/list', handler.testsList, 'show_tests_list'),
        
        # Инфомация о тесте
        ('GET', '/api/tests/{testid}/info', handler.testInfo, 'show_test_info'),
        
        # Инфомация о тесте
        ('GET', '/api/tests/{testid}/execution_history', handler.testExecutionHistory, 'show_execution_history')
    ]

    # Добавляем маршруты с обработчиками в приложение
    logger.debug('Add routes with handlers to the application...')
    for route in routes:
        try:
            app.router.add_route(route[0], route[1], route[2], name=route[3])
        except Exception as e:
            logger.error('Error add route: name - {}.'.format(route[3]))
            logger.error(e)

'''
# Показать всех пользователей из базы данных
('GET', '/api/user/list', handler.userList, 'show_users'),

# Показать пользователя в базе данных по номеру телефона
('GET', '/api/user/{username}/info', handler.userInfo, 'show_user'),

# Добавляем пользователя и свойсвтво
('POST', '/api/user/set', handler.addUser, 'add_user'),
'''