#!/usr/bin/python3

# Создать новое окружение:
# python3 -m venv <path>
# Активировать окружение:
# source /home/ttiimmoonn/python/venv/radius-api/bin/activate
# Использовать окружение:
# /home/ttiimmoonn/python/venv/radius-api/bin/python3 ./server-main.py


from aiohttp import web
import asyncio
from dotmap import DotMap

# Импортируем routes для api
from routes import setup_routes
# Испортируем логер и конфигурации сервера
from settings import config, logger


async def init_app():
    """Формируем настроки для сервера."""
    try:
        application = web.Application()
        application['config'] = DotMap(config)
        setup_routes(application, logger)
        return application

    except Exception as e:
        logger.error('Error create application.')
        logger.error(e)
        return False

if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        logger.info('Init app...')
        app = loop.run_until_complete(init_app())
        if not app:
            raise IOError("Errors run unit init app.")
        else:
            web.run_app(app, 
                host=config['host'], 
                port=config['port']
                )
    except Exception as ex:
        logger.error('Error run application.')
